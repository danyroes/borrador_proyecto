const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechudare6ed/collections/";
const mLabAPIKey = "apiKey=Ic1v0kegSp7qNlq4tD5poJvRLWRd6MwX";


function getAccountV1(req,res){
  console.log("GET /apitechu/v1/accounts/");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Client created');
  httpClient.get('account?'+mLabAPIKey,
    function(err, resMLab, body){
      var response = !err ? body : {
        "msg" : "Error obteniendo cuentas"
      }
      res.send(response);
    }
  );
}

function getAccountByIdV1(req,res){
  console.log("GET /apitechu/v1/accounts/:id");

  var id = req.params.id;
  var query = 'q={"id":' + id +'}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Client created');
  httpClient.get('account?'+query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if (err){
        response = {
          "msg" : "Error obteniendo cuentas"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          var response = body[0];
        }else{
          var response = {
            "msg" : "Cuenta no encontrada"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}



module.exports.getAccountV1 = getAccountV1;
module.exports.getAccountByIdV1 = getAccountByIdV1;
