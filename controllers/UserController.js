const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechudare6ed/collections/";
const mLabAPIKey = "apiKey=Ic1v0kegSp7qNlq4tD5poJvRLWRd6MwX";

/*
app.get('/apitechu/v1/users',
  function(req,res){
    console.log("GET /apitechu/v1/users");

    //res.sendFile('usuarios.json',{root: __dirname});
    var users = require('./usuarios.json');
    res.send(users);
  }
);
*/

function getUsersV1(req,res){
  console.log("GET /apitechu/v1/users/");

  //res.sendFile('usuarios.json',{root: __dirname});
  var users = require('../usuarios.json');

  if (req.query.$top != undefined){
    var usersTop = new Array();
    var index=0;
    for (user of users) {
         console.log("Length of array is " + users.length);
         console.log("top is " + req.query.$top);
         console.log("count is " + req.query.$count);
         if (user != null && index < req.query.$top) {
           console.log("El top coincide");
           usersTop[index]= user;
         }else{
            if (req.query.$count == 'true'){
              var count = {"Count" : users.length};
              usersTop[index]= count;
            }
            break;
         }

         index += 1;
       }
     }else{
       var usersTop= require('../usuarios.json');
       console.log("No hay queryParam " + usersTop.length)
     }

  res.send(usersTop);

  /*
  Aquí poner el código del profe
  var result = {};
  var users = require('../usuarios.json');
  */

}


function createUserV1(req,res){
  console.log("POST /apitechu/v1/users");
  console.log("First_name is: " + req.body.first_name);
  console.log("Last_name is: " + req.body.last_name);
  console.log("Email is: " + req.body.email);


  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email is" : req.body.email
  };

  var users = require('../usuarios.json');
  users.push(newUser);

  io.writeUserDataToFile(users);

  console.log("Usuario añadido con éxito");

  res.send({"msg":"Usuario añadido con éxito"});
}

function createUserV2(req,res){
  console.log("POST /apitechu/v2/users");
  console.log("Id: " + req.body.id);
  console.log("First_name is: " + req.body.first_name);
  console.log("Last_name is: " + req.body.last_name);
  console.log("Email is: " + req.body.email);
  //console.log("password: " + req.body.password);


  var newUser = {
    "id" : req.body.id,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : crypt.hash(req.body.password)
  };

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Client created');
  httpClient.post('user?'+mLabAPIKey,newUser,
    function(err, resMLab, body){
      console.log("Usuario creado con éxito");
      res.send({"msg" : "Usuario creado con éxito"})
    }
  );

}


function deleteUserV1(req,res){
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("id es " + req.params.id);

  var users = require('../usuarios.json');
  //users.splice(req.params.id - 1,1); //le quita los datos que se le indica por parametro y devuelve la lista reducida
/*
  var index=0;
  let iterable = users;
  for (let user of iterable){
    console.log(iterable[index].id);
    if (iterable[index].id == req.params.id){
      console.log("El usuario borrado es el " + iterable[index].first_name);
      console.log("Ha llegado a la iteración " + index);

      //delete users[user.id-1];
      users.splice(index,1);
      break;
    }
    value += 1;
  }
  */
  var msg = "Usuario no borrado";
  console.log("Length of array is " + users.length);
  for (user of users) {
     if (user != null && user.id == req.params.id) {
       console.log("La id coincide");
       console.log("El usuario borrado es el " + user.first_name);
       delete users[user.id];
       //users.splice(user.id - 1, 1);
       msg = "Usuario borrado";
       break;
     }
   }


  io.writeUserDataToFile(users);

  console.log(msg);
}

function getUsersV2(req,res){
  console.log("GET /apitechu/v2/users/");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Client created');
  httpClient.get('user?'+mLabAPIKey,
    function(err, resMLab, body){
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }
  );
}

function getUserByIdV2(req,res){
  console.log("GET /apitechu/v2/users/:id");

  var id = req.params.id;
  var query = 'q={"id":' + id +'}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Client created');
  httpClient.get('user?'+query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if (err){
        response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          var response = body[0];
        }else{
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}

function getAccountsV2(req,res){
  console.log("GET /apitechu/v2/users/:id/accounts");

  var id = req.params.id;
  var query = 'q={"userid":' + id +'}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Client created');
  httpClient.get('account?'+query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if (err){
        response = {
          "msg" : "Error obteniendo cuentas"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          var response = body;
        }else{
          var response = {
            "msg" : "Cuenta no encontrada"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.getAccountsV2 = getAccountsV2;
