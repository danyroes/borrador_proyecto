const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require('body-parser');

app.use(bodyParser.json());

app.listen(port);
console.log("API escuchando en el puerto" + port);

const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');

//////////////////////////// Práctica1 inicio
app.get('/apitechu/v1/users', userController.getUsersV1);
//////////////////////////// Práctica1 fin
app.post('/apitechu/v1/users',userController.createUserV1);
app.post('/apitechu/v2/users',userController.createUserV2);
app.delete('/apitechu/v1/users/:id',userController.deleteUserV1);
app.get('/apitechu/v2/users',userController.getUsersV2);
app.get('/apitechu/v2/users/:id',userController.getUserByIdV2);

//////////////////////////// Práctica2 Login y Logout - inicio
app.post('/apitechu/v1/login',authController.logInV1);
app.post('/apitechu/v1/logout',authController.logOutV1);
//////////////////////////// Práctica2 Login y Logout - fin
app.post('/apitechu/v2/login',authController.logInV2);
app.post('/apitechu/v2/logout/:id',authController.logOutV2);

app.get('/apitechu/v1/users/:id/accounts',userController.getAccountsV2);
app.get('/apitechu/v1/accounts/',accountController.getAccountV1);
app.get('/apitechu/v1/accounts/:id',accountController.getAccountByIdV1);



app.get('/apitechu/v1/hello',
  function(req,res){
    console.log("GET /apitechu/v1/hello");

    res.send({"msg":"Hola desde API TechU en el contenedor Docker - actualización automática_v2"});
  }
);


app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req,res){
    console.log("POST /apitechu/v1/monstruo/:p1/:p2");

    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);

  }
)
